import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl('http://192.168.13.21:9632/#/')

WebUI.setText(findTestObject('Object Repository/Tax Master/Page_NexxRetail - Next Generation Retail Ap_096274/input_Next_userName'), 
    'mudavadi')

WebUI.click(findTestObject('Object Repository/Tax Master/Page_NexxRetail - Next Generation Retail Ap_096274/button_Next'))

WebUI.setEncryptedText(findTestObject('Object Repository/Tax Master/Page_NexxRetail - Next Generation Retail Ap_096274/input_Login_password'), 
    '4nvbrPglk7k=')

WebUI.click(findTestObject('Object Repository/Tax Master/Page_NexxRetail - Next Generation Retail Ap_096274/button_Login'))

WebUI.click(findTestObject('others/Page_NexxRetail - Next Generation Retail Application/span_Tax'))

WebUI.click(findTestObject('others/Page_NexxRetail - Next Generation Retail Application/span_Master'))

WebUI.click(findTestObject('Object Repository/Tax Master/Page_NexxRetail - Next Generation Retail Ap_096274/button_New'))

WebUI.setText(findTestObject('Object Repository/Tax Master/Page_NexxRetail - Next Generation Retail Ap_096274/input_Code_code'), 
    'TM1004')

WebUI.setText(findTestObject('Object Repository/Tax Master/Page_NexxRetail - Next Generation Retail Ap_096274/input_Name_name'), 
    'Tmaster2')

WebUI.click(findTestObject('Object Repository/Tax Master/Page_NexxRetail - Next Generation Retail Ap_096274/span_Fixed Amount_fa fa-circle'))

WebUI.setText(findTestObject('Object Repository/Tax Master/Page_NexxRetail - Next Generation Retail Ap_096274/input_Tax Percentage_defaultTax'), 
    '0')

WebUI.click(findTestObject('Object Repository/Tax Master/Page_NexxRetail - Next Generation Retail Ap_096274/span_Output Tax_fa fa-circle'))

WebUI.click(findTestObject('Object Repository/Tax Master/Page_NexxRetail - Next Generation Retail Ap_096274/button_Save'))

