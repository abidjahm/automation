import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

not_run: WebUI.openBrowser('')

not_run: WebUI.navigateToUrl('http://192.168.13.21:9632/#/')

not_run: WebUI.setText(findTestObject('Object Repository/Setup/Page_NexxRetail - Next Generation Retail Ap_096274/input_Next_userName'), 
    'admin')

not_run: WebUI.click(findTestObject('Object Repository/Setup/Page_NexxRetail - Next Generation Retail Ap_096274/button_Next'))

not_run: WebUI.setEncryptedText(findTestObject('Object Repository/Setup/Page_NexxRetail - Next Generation Retail Ap_096274/input_Login_password'), 
    '4nvbrPglk7k=')

not_run: WebUI.click(findTestObject('Object Repository/Setup/Page_NexxRetail - Next Generation Retail Ap_096274/button_Login'))

WebUI.callTestCase(findTestCase('Inventory'), [:], FailureHandling.OPTIONAL)

WebUI.delay(5)

WebUI.click(findTestObject('Object Repository/Setup/Page_NexxRetail - Next Generation Retail Ap_096274/em_Submit_icon-grid sizeforsmalldevice'))

WebUI.click(findTestObject('Object Repository/Setup/Page_NexxRetail - Next Generation Retail Ap_096274/a_Setup'))

WebUI.click(findTestObject('Object Repository/Setup/Page_NexxRetail - Next Generation Retail Ap_096274/span_Settings'))

WebUI.click(findTestObject('Object Repository/Setup/Page_NexxRetail - Next Generation Retail Ap_096274/span_Global Policies'))

WebUI.click(findTestObject('Object Repository/Setup/Page_NexxRetail - Next Generation Retail Ap_096274/button_Edit'))

WebUI.click(findTestObject('Object Repository/Setup/Page_NexxRetail - Next Generation Retail Ap_096274/button_Save'))

WebUI.click(findTestObject('Object Repository/Setup/Page_NexxRetail - Next Generation Retail Ap_096274/button_Edit'))

WebUI.click(findTestObject('Object Repository/Setup/Page_NexxRetail - Next Generation Retail Ap_096274/button_Save'))

