import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys


WebUI.openBrowser('http://192.168.13.21:9632/#/')

WebUI.setText(findTestObject('Merchandising/Page_NexxRetail - Next Generation Retail Application/input_Next_userName'), 
    'admin')

WebUI.click(findTestObject('Merchandising/Page_NexxRetail - Next Generation Retail Application/button_Next'))

WebUI.setEncryptedText(findTestObject('Merchandising/Page_NexxRetail - Next Generation Retail Application/input_Login_password'), 
    '4nvbrPglk7k=')

WebUI.click(findTestObject('Merchandising/Page_NexxRetail - Next Generation Retail Application/button_Login'))

WebUI.click(findTestObject('Merchandising/Page_NexxRetail - Next Generation Retail Application/span_Vendor Master'))

WebUI.click(findTestObject('Merchandising/Page_NexxRetail - Next Generation Retail Application/button_New'))

WebUI.click(findTestObject('Merchandising/Page_NexxRetail - Next Generation Retail Application/input'))

WebUI.setText(findTestObject('Merchandising/Page_NexxRetail - Next Generation Retail Application/input'), 'Kenya')

WebUI.sendKeys(findTestObject(null), Keys.chord(Keys.ENTER))

//WebUI.sendKeys(findTestObject(null), Keys.chord(Keys.ENTER))
WebUI.click(findTestObject('Merchandising/Page_NexxRetail - Next Generation Retail Application/em_Submit_icon-logout sizeforsmalldevice'))

WebUI.delay(4)

