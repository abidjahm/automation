import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('http://192.168.13.198:9632/#/')

WebUI.setText(findTestObject('Object Repository/Sale with Cash Credit Card Cheque/Page_NexxRetail - Next Generation Retail Ap_096274/input_Next_userName'), 
    'antony')

WebUI.click(findTestObject('Object Repository/Sale with Cash Credit Card Cheque/Page_NexxRetail - Next Generation Retail Ap_096274/button_Next'))

WebUI.setEncryptedText(findTestObject('Object Repository/Sale with Cash Credit Card Cheque/Page_NexxRetail - Next Generation Retail Ap_096274/input_Login_password'), 
    'iGDxf8hSRT4=')

WebUI.click(findTestObject('Object Repository/Sale with Cash Credit Card Cheque/Page_NexxRetail - Next Generation Retail Ap_096274/button_Login'))

not_run: WebUI.click(findTestObject('Object Repository/Sale with Cash Credit Card Cheque/Page_NexxRetail - Next Generation Retail Ap_096274/button_Yes'))

WebUI.click(findTestObject('Sale with Cash Credit Card Cheque/Page_NexxRetail - Next Generation Retail Application/em_Submit_icon-grid sizeforsmalldevice'))

WebUI.click(findTestObject('Object Repository/Sale with Cash Credit Card Cheque/Page_NexxRetail - Next Generation Retail Ap_096274/a_Web POS'))

WebUI.click(findTestObject('Object Repository/Sale with Cash Credit Card Cheque/Page_NexxRetail - Next Generation Retail Ap_096274/button_OK'))

WebUI.click(findTestObject('Sale with Cash Credit Card Cheque/Page_NexxRetail - Next Generation Retail Application/span_AHMEDABAD CANADA'))

WebUI.click(findTestObject('Object Repository/Sale with Cash Credit Card Cheque/Page_NexxRetail - Next Generation Retail Ap_096274/a_Delhi'))

WebUI.click(findTestObject('Object Repository/Sale with Cash Credit Card Cheque/Page_NexxRetail - Next Generation Retail Ap_096274/span_Sale'))

WebUI.click(findTestObject('sdi/Page_NexxRetail - Next Generation Retail Application/input'))

WebUI.setText(findTestObject('sdi/Page_NexxRetail - Next Generation Retail Application/input'), 'Corporate')

WebUI.sendKeys(findTestObject(null), Keys.chord(Keys.ENTER))

WebUI.click(findTestObject('2/Page_NexxRetail - Next Generation Retail Application/input'))

WebUI.setText(findTestObject('2/Page_NexxRetail - Next Generation Retail Application/input'), 'Payment Gateway')

WebUI.sendKeys(findTestObject(null), Keys.chord(Keys.ENTER))

not_run: WebUI.click(findTestObject('Sale with Cash Credit Card Cheque/Page_NexxRetail - Next Generation Retail Ap_096274/button_OK'))

WebUI.click(findTestObject('Object Repository/Sale with Cash Credit Card Cheque/Page_NexxRetail - Next Generation Retail Ap_096274/p_Aig'))

WebUI.setText(findTestObject('pd/Page_NexxRetail - Next Generation Retail Application/input_Sell Price_sellPrice'), '40000')

WebUI.click(findTestObject('Sale with Cash Credit Card Cheque/Page_NexxRetail - Next Generation Retail Application/button_OK'))

WebUI.delay(2)

WebUI.setText(findTestObject('Object Repository/Sale with Cash Credit Card Cheque/Page_NexxRetail - Next Generation Retail Ap_096274/input__attr00'), 
    'a@yahoo.com')

WebUI.setText(findTestObject('Object Repository/Sale with Cash Credit Card Cheque/Page_NexxRetail - Next Generation Retail Ap_096274/input__attr10'), 
    'name')

WebUI.setText(findTestObject('Object Repository/Sale with Cash Credit Card Cheque/Page_NexxRetail - Next Generation Retail Ap_096274/input__attr20'), 
    '0780001200')

WebUI.setText(findTestObject('Object Repository/Sale with Cash Credit Card Cheque/Page_NexxRetail - Next Generation Retail Ap_096274/input__attr30'), 
    '1234')

WebUI.click(findTestObject('Page_NexxRetail - Next Generation Retail Application/button_OK'))

WebUI.click(findTestObject('Object Repository/Sale with Cash Credit Card Cheque/Page_NexxRetail - Next Generation Retail Ap_096274/p_Axin insurance'))

WebUI.click(findTestObject('axin/Page_NexxRetail - Next Generation Retail Application/button_OK'))

WebUI.setText(findTestObject('Sale with Cash Credit Card Cheque/Page_NexxRetail - Next Generation Retail Application/input'), 
    'Kenya')

WebUI.sendKeys(findTestObject(null), Keys.chord(Keys.ENTER))

WebUI.setText(findTestObject('lity/Page_NexxRetail - Next Generation Retail Application/input'), 'Kenyan')

WebUI.sendKeys(findTestObject(null), Keys.chord(Keys.ENTER))

WebUI.setText(findTestObject('Sale with Cash Credit Card Cheque/Page_NexxRetail - Next Generation Retail Application/input__form-control tenant ng-untouched ng-pristine ng-valid ng-star-inserted'), 
    '9876')

WebUI.setText(findTestObject('Sale with Cash Credit Card Cheque/Page_NexxRetail - Next Generation Retail Application/input_Customer Address_form-control tenant ng-untouched ng-pristine ng-valid ng-star-inserted'), 
    'address1')

WebUI.click(findTestObject('Object Repository/Sale with Cash Credit Card Cheque/Page_NexxRetail - Next Generation Retail Ap_096274/button_Proceed To Payment'))

WebUI.click(findTestObject('a/Page_NexxRetail - Next Generation Retail Application/button_Yes'))

WebUI.delay(2)

WebUI.setText(findTestObject('a/Page_NexxRetail - Next Generation Retail Application/input_Discount_payAmountInput'), '40020')

WebUI.click(findTestObject('a/Page_NexxRetail - Next Generation Retail Application/span_Enter'))

WebUI.click(findTestObject('a/Page_NexxRetail - Next Generation Retail Application/button_Pay'))

WebUI.delay(3)

WebUI.closeWindowIndex(1)

WebUI.switchToWindowIndex(0)

