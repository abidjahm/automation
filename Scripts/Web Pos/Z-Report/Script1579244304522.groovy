import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('http://192.168.13.198:9632/#/')

WebUI.setText(findTestObject('Object Repository/Sale with Cash Credit Card Cheque/Page_NexxRetail - Next Generation Retail Ap_096274/input_Next_userName'), 
    'antony')

WebUI.click(findTestObject('Object Repository/Sale with Cash Credit Card Cheque/Page_NexxRetail - Next Generation Retail Ap_096274/button_Next'))

WebUI.setEncryptedText(findTestObject('Object Repository/Sale with Cash Credit Card Cheque/Page_NexxRetail - Next Generation Retail Ap_096274/input_Login_password'), 
    'iGDxf8hSRT4=')

WebUI.click(findTestObject('Object Repository/Sale with Cash Credit Card Cheque/Page_NexxRetail - Next Generation Retail Ap_096274/button_Login'))

not_run: WebUI.click(findTestObject('Sale with Cash Credit Card Cheque/Page_NexxRetail - Next Generation Retail Ap_096274/button_Yes'))

WebUI.click(findTestObject('Sale with Cash Credit Card Cheque/Page_NexxRetail - Next Generation Retail Application/em_Submit_icon-grid sizeforsmalldevice'))

WebUI.click(findTestObject('Object Repository/Sale with Cash Credit Card Cheque/Page_NexxRetail - Next Generation Retail Ap_096274/a_Web POS'))

WebUI.click(findTestObject('Object Repository/Sale with Cash Credit Card Cheque/Page_NexxRetail - Next Generation Retail Ap_096274/button_OK'))

WebUI.click(findTestObject('Sale with Cash Credit Card Cheque/Page_NexxRetail - Next Generation Retail Application/span_AHMEDABAD CANADA'))

WebUI.click(findTestObject('Object Repository/Sale with Cash Credit Card Cheque/Page_NexxRetail - Next Generation Retail Ap_096274/a_Delhi'))

WebUI.click(findTestObject('zed/Page_NexxRetail - Next Generation Retail Application/span_Zed Report'))

WebUI.click(findTestObject('zed/Page_NexxRetail - Next Generation Retail Application/button_Save'))

WebUI.delay(3)

WebUI.closeWindowIndex(1)

WebUI.switchToWindowIndex(0)

not_run: WebUI.setText(findTestObject('zed/Page_NexxRetail - Next Generation Retail Application/input_Cash_declarationValue'), 
    '200')

not_run: WebUI.setText(findTestObject('zed/Page_NexxRetail - Next Generation Retail Application/input_Credit Card_declarationValue'), 
    '1000')

not_run: WebUI.setText(findTestObject('zed/Page_NexxRetail - Next Generation Retail Application/input_Cheque_declarationValue'), 
    '2000')

not_run: WebUI.setText(findTestObject('zed/Page_NexxRetail - Next Generation Retail Application/input_Credit Note_declarationValue'), 
    '1000')

